import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.requests.GatewayIntent;

import javax.security.auth.login.LoginException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


//Created by Suleman Akhter - EmpiresBot

public class Main {

    public static JDABuilder builder;
    public static String prefix = ".";
    public static ArrayList<Empire> list = new ArrayList<Empire>();
    public static ArrayList<TextChannel> permChannel = new ArrayList<TextChannel>();
    public static boolean joinMess = false;
    public static String ID;
    public static TextChannel announcement;







    public static void main(String[] args) throws LoginException {


        String token = ""; //This token only to be added when activating bot
        builder = JDABuilder.createDefault(token);
        builder.setActivity(Activity.playing("Empire Bot"));
        builder.setBulkDeleteSplittingEnabled(false);
        builder.enableIntents(GatewayIntent.GUILD_MEMBERS);
        registerListeners();
        builder.build();


    }

    public static void registerListeners(){


       builder.addEventListeners(new Command());



    }

    static class Empire{
        Role leader;
        Role role;
        String name;
        int score;
        int memberCount;
        Date start;
        Date end;
        boolean cooldown = false;
        String flag;

        Empire(Role r, String n, int s, int c){
            role = r;
            name = n;
            score = s;
            memberCount = c;
            leader = null;



        }

        public void setLeader(Role l){

            leader = l;

        }
        public void setFlag (String link){

            flag = link;

        }




    }



}

