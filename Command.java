import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class Command extends ListenerAdapter {


    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        String[] arg;
        arg = event.getMessage().getContentRaw().split(" ");
        List<Role> a = event.getMessage().getMentionedRoles();
        List<TextChannel> b = event.getMessage().getMentionedChannels();
        TextChannel current = event.getChannel();
        int cooldownTime = 1440;

        String full = "";
        String fullEmp = "";
        String endDate = null;

        //Commands Handle
        if (arg[0].equalsIgnoreCase(Main.prefix + "info")) {

            EmbedBuilder alpha = new EmbedBuilder();
            alpha.setTitle("Command Information");
            alpha.setFooter("Developed by Halal Lounge");
            alpha.setDescription("**User Commands:**\n" +
                    "\n" +
                    ".lb -> Displays the leaderboard\n" +
                    "\n" +
                    ".offduty -> Removes all of your empire roles\n" +
                    "\n" +
                    ".join <empire name> -> Allows you to join an empire given the name (Not Role Ping)\n" +
                    "\n" +
                    ".sabotage <empire name> -> Allows you to sabotage an empire (Only can be used by Leaders)\n" +
                    "\n" +
                    ".sabotage info -> Shows you sabotage information\n" +
                    "\n" +
                    ".empire list -> Lists all of the current empires\n" +
                    "\n" +
                    ".channel list -> lists all of the permitted channels\n" +
                    "\n" +
                    ".leader list -> Lists all of the associated leader role for each empire\n" +
                    "\n" +
                    "\n" +
                    "**Admin Commands:**\n" +
                    "\n" +
                    ".add empire <@empire role> -> Allows you to add an empire" +
                    "\n" +
                    ".remove empire <@empire role> -> Allows you to remove an empire\n" +
                    "\n" +
                    ".add channel <#channel> -> Allows you to add an channel where points can be gained from\n" +
                    "\n" +
                    ".remove channel <#channel> -> Allows you to remove a channel where points can be gained from\n" +
                    "\n" +
                    ".add leader <@empire role> <@leader role> -> Allows you to add a leader for some empire\n" +
                    "\n" +
                    ".remove cooldown <empire name> -> Allows you to remove the cool down for that empire\n" +
                    "\n" +
                    ".increment <number> <empire name> -> Allows you to increment the number of points for that empire\n" +
                    "\n" +
                    ".decrement <number> <empire name> -> Allows you to decrement the number of points for that empire\n" +
                    "\n" +
                    ".reset settings -> Resets all of the settings\n" +
                    "\n" +
                    ".announcement <#channel> -> Sets the announcement channel for the bot");

            alpha.setColor(0xebf57c);
            event.getChannel().sendMessage(alpha.build()).queue();


        } else if (arg[0].equalsIgnoreCase(Main.prefix + "add") && arg.length > 1 && arg[1].equalsIgnoreCase("empire") && event.getMember().hasPermission(Permission.ADMINISTRATOR)){

             if (a.size() <= 0 || a.size() > 1) {

                event.getChannel().sendMessage("Invalid Command, please add only 1 existing role").queue();

            }

            else {


                for(int i = 0; i<Main.list.size(); i++){


                    if(Main.list.get(i).leader != null && Main.list.get(i).leader.equals(a.get(0))){

                        Main.list.get(i).leader = null;

                    }


                }


                boolean c = true;
                for (int i = 0; i < Main.list.size(); i++) {

                    if (Main.list.get(i).role.equals(a.get(0))) {
                        c = false;
                        event.getChannel().sendMessage("This role has already been added").queue();
                        break;

                    }

                }


                if (c == true) {
                    for (int i = 2; i < arg.length; i++) {

                        full = full + arg[i];

                    }



                    Main.list.add(new Main.Empire(a.get(0), full, 0, 0));
                    event.getChannel().sendMessage(full + " added!").queue();
                }
            }

        }
        else if (arg[0].equalsIgnoreCase(Main.prefix + "prefix") && event.getMember().hasPermission(Permission.ADMINISTRATOR)){

            if(arg.length == 2) {
                Main.prefix = arg[1];
                event.getChannel().sendMessage("Successfully changed prefix to ``" + Main.prefix + "``").queue();


            }
            else{

                event.getChannel().sendMessage("Current prefix: ``" + Main.prefix + "``").queue();

            }


        }

        else if (arg[0].equalsIgnoreCase(Main.prefix + "remove") && arg.length > 1 && arg[1].equalsIgnoreCase("empire") &&  event.getMember().hasPermission(Permission.ADMINISTRATOR)) {

            if (a.size() <= 0 || a.size() > 1) {

                event.getChannel().sendMessage("Invalid Command, please add 1 role to remove").queue();

            } else {
                boolean c = true;
                for (int i = 0; i < Main.list.size(); i++) {

                    if (Main.list.get(i).role.equals(a.get(0))) {

                        Main.list.remove(i);
                        event.getChannel().sendMessage("Role has been removed!").queue();
                        break;


                    } else if (i == Main.list.size() - 1) {

                        event.getChannel().sendMessage("Invalid, role does not exist").queue();

                    }
                }
            }
        } else if (arg[0].equalsIgnoreCase(Main.prefix + "lb")) {

            String url = event.getGuild().getIconUrl();
            String name = event.getGuild().getName();
            EmbedBuilder lb = new EmbedBuilder();
            lb.setTitle("Empire Leaderboard");
            lb.setAuthor(name);
            lb.setThumbnail(url);
            boolean condition = true;
            int highestScore = 0;
            int highestIndex = -1;


            //Copying the List
            ArrayList<Main.Empire> copy = new ArrayList<>(Main.list);
            ArrayList<Main.Empire> biggestInOrder = new ArrayList<>();


            Boolean notempty = true;

            System.out.println("TESTING");
            System.out.println(copy.size());


            while(notempty) {

                int biggestScore = 0;
                int biggestIndex = 0;

                System.out.println(copy.size());
                for (int i = 0; i < copy.size(); i++) {

                    if (copy.get(i).score > biggestScore) {
                        biggestScore = copy.get(i).score;
                        biggestIndex = i;
                    }
                }
                if(copy.size() == 0){
                    notempty = false;
                }
                else {
                    biggestInOrder.add(copy.get(biggestIndex));
                    copy.remove(biggestIndex);
                }
            }
            if(biggestInOrder.size() == 0) {
                event.getChannel().sendMessage("Invalid, no empires exist").queue();
                condition = false;
            }
            else {
                for (int i = 0; i < biggestInOrder.size(); i++) {

                    if (i == 0) {
                        lb.setColor(biggestInOrder.get(i).role.getColor());
                        lb.addField("\uD83C\uDFC6 -> " + biggestInOrder.get(i).role.getName(), "Score " + biggestInOrder.get(i).score, false);
                        if(biggestInOrder.get(i).flag != null){
                            lb.setThumbnail(biggestInOrder.get(i).flag);
                        }
                    } else {
                        lb.addField("-> " + biggestInOrder.get(i).role.getName(), "Score " + biggestInOrder.get(i).score, false);

                    }

                }
            }







            if (condition == true) {
                event.getChannel().sendMessage(lb.build()).queue();
            }


        } else if (arg[0].equalsIgnoreCase(Main.prefix + "offduty")) {
            boolean condition = false;
            int v = 0;
            List<Role> listofRole = event.getMember().getRoles();
            for (int d = 0; d < Main.list.size(); d++) {
                for (int i = 0; i < listofRole.size(); i++) {

                    if (listofRole.get(i).equals(Main.list.get(d).role)) {

                        v = d;
                        event.getMember().getGuild().removeRoleFromMember(event.getMember(), Main.list.get(d).role).queue();
                        condition = true;

                        break;
                    }


                }
            }

            if (condition == true) {

                EmbedBuilder alpha = new EmbedBuilder();
                alpha.setTitle("Removed Empire Roles!");
                alpha.setColor(Main.list.get(v).role.getColor());
                alpha.setDescription("Thanks for participating ");
                alpha.setImage("https://media.giphy.com/media/iPiUxztIL4Sl2/giphy.gif");
                alpha.setFooter("Type .info for list of commands");



                event.getChannel().sendMessage(alpha.build()).queue();
            } else {
                EmbedBuilder alpha = new EmbedBuilder();
                alpha.setTitle("You're not in any empires!");
                alpha.setColor(0xed4c37);
                alpha.setDescription("You already do not have any empire roles");
                alpha.setFooter("Type .info for list of commands");
                event.getChannel().sendMessage(alpha.build()).queue();

            }


        }
        else if (arg[0].equalsIgnoreCase(Main.prefix + "flag")  && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {

            if( a.size() != 1 || arg.length != 3){

                event.getChannel().sendMessage("Incorrect Format. flag @empire link").queue();
            }
            else {
                for (int i = 0; i < Main.list.size(); i++) {

                    if (Main.list.get(i).role.equals(a.get(0))) {

                        Main.list.get(i).setFlag(arg[2]);
                        event.getChannel().sendMessage("Successfully added flag for: " + Main.list.get(i).name).queue();
                        break;


                    }
                    else if(i == Main.list.size() - 1){
                        event.getChannel().sendMessage("Empire role does not exist" + Main.list.get(i).name).queue();

                    }

                }
            }

        }
        else if (arg[0].equalsIgnoreCase(Main.prefix + "add") && arg.length > 1 && arg[1].equalsIgnoreCase("leader") && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {

            boolean condition = false;
            boolean condition2 = false;
            if (a.size() == 0) {

                event.getChannel().sendMessage("No roles have been mentioned.").queue();
            } else if (a.size() > 2) {

                event.getChannel().sendMessage("Too many roles have been mentioned, please mention the empire role then the leader role.").queue();

            } else if (a.size() == 1) {

                event.getChannel().sendMessage("Invalid, you have to mention two different roles.").queue();

            } else {
                Role first = a.get(0);
                Role second = a.get(1);
                int index = -1;
                for (int i = 0; i < Main.list.size(); i++) {

                    if (Main.list.get(i).role.equals(first)) {

                        condition = true;
                        index = i;

                    }

                }

                for (int i = 0; i < Main.list.size(); i++) {
                    if (Main.list.get(i).role.equals(second)) {

                        event.getChannel().sendMessage("Invalid, the second role you have mentioned is an empire role, please type a leadership role for this empire").queue();
                        condition = false;
                        break;
                    }
                }


                if (condition == true) {


                    for (int i = 0; i < Main.list.size(); i++) {


                        if (Main.list.get(i).leader != null && Main.list.get(i).leader.equals(second)) {

                            Main.list.get(i).leader = null;


                        }
                    }

                    Main.list.get(index).setLeader(second);
                    event.getChannel().sendMessage("Successfully added the leader role for this empire").queue();

                }

            }

        } else if (arg[0].equalsIgnoreCase(Main.prefix + "leader") && arg.length > 1 && arg[1].equalsIgnoreCase("list")) {
            String leader;
            if (Main.list.size() == 0) {

                event.getChannel().sendMessage("No empires have been declared").queue();

            } else {
                for (int i = 0; i < Main.list.size(); i++) {

                    if (Main.list.get(i).leader == null) {

                        leader = "Not set";

                    } else {

                        leader = Main.list.get(i).leader.getAsMention();

                    }
                    fullEmp = (Main.list.size() - i) + ". " + Main.list.get(i).role.getAsMention() + " -> " + leader + "\n" + fullEmp;

                }
                EmbedBuilder bravo = new EmbedBuilder();
                bravo.setColor(0xebf57c);
                bravo.setTitle("Leader List");
                bravo.setDescription(fullEmp);
                bravo.setFooter("Type .info for list of commands");
                event.getChannel().sendMessage(bravo.build()).queue();
            }


        }
        else if (arg[0].equalsIgnoreCase(Main.prefix + "list") && arg.length > 1 && arg[1].equalsIgnoreCase("flag")) {
            String checkFlag;
            if (Main.list.size() == 0) {

                event.getChannel().sendMessage("No empires have been declared").queue();

            } else {
                for (int i = 0; i < Main.list.size(); i++) {

                    if (Main.list.get(i).flag == null) {

                        checkFlag = "Not set";

                    } else {

                        checkFlag = Main.list.get(i).flag;

                    }
                    fullEmp = (Main.list.size() - i) + ". " + Main.list.get(i).role.getAsMention() + " -> " + checkFlag + "\n" + fullEmp;

                }
                EmbedBuilder bravo = new EmbedBuilder();
                bravo.setColor(0xebf57c);
                bravo.setTitle("Flag list");
                bravo.setDescription(fullEmp);
                bravo.setFooter("Type .info for list of commands");
                event.getChannel().sendMessage(bravo.build()).queue();
            }


        }else if (arg[0].equalsIgnoreCase(Main.prefix + "join")) {
            boolean condition = true;


            if (Main.list.size() == 0) {

                event.getChannel().sendMessage("No empires exist to join").queue();

            } else {


                List<Role> listofRole = event.getMember().getRoles();

                for (int d = 0; d < Main.list.size(); d++) {
                    for (int i = 0; i < listofRole.size(); i++) {

                        if (listofRole.get(i).equals(Main.list.get(d).role)) {

                            condition = false;
                            break;
                        }


                    }
                }
            }

            if (condition == false) {

                EmbedBuilder alpha = new EmbedBuilder();
                alpha.setTitle("Already in Empire");
                alpha.setColor(0xed4c37);

                alpha.setDescription("- You are already in an empire!\n- To remove current empire type ``.offduty``");
                alpha.setFooter("Type .info for list of commands");
                event.getChannel().sendMessage(alpha.build()).queue();

            } else {

                StringBuilder sb = new StringBuilder();
                for (int i = 1; i < arg.length; i++) {
                    if (i == arg.length - 1) {

                        sb.append(arg[i]);

                    } else {
                        sb.append(arg[i] + " ");
                    }
                }

                full = sb.toString();



                for (int i = 0; i < Main.list.size(); i++) {


                    if (full.equalsIgnoreCase(Main.list.get(i).role.getName())) {
                        EmbedBuilder bravo = new EmbedBuilder();
                        bravo.setColor(Main.list.get(i).role.getColor());
                        bravo.setTitle("Successfully Joined! ");
                        bravo.setDescription("- Welcome " +  " to the " + Main.list.get(i).role.getAsMention() + "\n - Gather points by simply talking" );
                        if(Main.list.get(i).flag != null){
                            bravo.setImage(Main.list.get(i).flag);
                        }
                        else {
                            bravo.setImage("https://media.giphy.com/media/CjmvTCZf2U3p09Cn0h/giphy.gif");
                        }
                        bravo.setFooter("Type .info for list of commands");
                        event.getMember().getGuild().addRoleToMember(event.getMember(), Main.list.get(i).role).queue();
                        event.getChannel().sendMessage(bravo.build()).queue();


                        break;
                    }

                    if (i == Main.list.size() - 1) {


                        EmbedBuilder alpha = new EmbedBuilder();
                        alpha.setTitle("Empire does not exist");
                        alpha.setColor(0xed4c37);

                        alpha.setDescription("- This empire does not exist \n- For a list of empires type ``.empire list``");
                        alpha.setFooter("Type .info for list of commands");

                        event.getChannel().sendMessage(alpha.build()).queue();

                    }

                }

            }

        }
        else if (arg[0].equalsIgnoreCase(Main.prefix + "increment") && arg.length > 1 && arg[1].equalsIgnoreCase("cooldown")) {
            //INCREMENTING FROM CURRENT TIME


            if(arg.length == 4 && a.size() == 1){

              //Everything correct

              for(int i = 0; i<Main.list.size(); i++){

                  if(a.get(0).equals(Main.list.get(i).role)){

                      if(Main.list.get(i).cooldown == false){
                          event.getChannel().sendMessage("Cooldown already not activated for that empire").queue();
                      }
                      else{
                          if (arg[2] != null){

                              try{
                                  Date date = new Date();
                                  int value = Integer.parseInt(arg[3]);
                                  Calendar c = Calendar.getInstance();
                                  c.setTime(date);
                                  c.add(Calendar.MINUTE,value);
                                  if(value > 0){
                                      Main.list.get(i).cooldown = true;
                                      Main.list.get(i).end = c.getTime();
                                  }
                                  event.getChannel().sendMessage("Successfully Changed!").queue();

                              }
                              catch (Exception e){
                                  event.getChannel().sendMessage("Invalid format, please ensure the value is a number with no decimal places.").queue();

                              }

                          }



                      }

                  }

              }

            }
            else{
                event.getChannel().sendMessage("Invalid format, .increment cooldown <integer>").queue();

            }

        }

        else if (arg[0].equalsIgnoreCase(Main.prefix + "sabotage")) {

            int chosenEmpire = -1;
            List<Role> listofRole = event.getMember().getRoles();
            int count = 0;
            String fromEmpire = "";

            if (arg.length > 1 && arg[1].equalsIgnoreCase("info")) {


                EmbedBuilder bravo = new EmbedBuilder();
                bravo.setColor(0xebf57c);
                bravo.setTitle("Sabotage Information");

                bravo.addField("-> 9% chance", "*This will deduct all points from the chosen team*", false);
                bravo.addField("-> 30% chance", "*This will deduct 2000 points from the chosen team*", false);
                bravo.addField("-> 60% chance", "*This will deduct 1000 points from the chosen team*", false);
                bravo.addField("-> 2% Chance", "*Nothing happens*", false);
                bravo.setFooter("Type .info for list of commands");
                event.getChannel().sendMessage(bravo.build()).queue();

            } else {


                for (int d = 0; d < Main.list.size(); d++) {
                    for (int i = 0; i < listofRole.size(); i++) {

                        if (Main.list.get(d).leader != null && Main.list.get(d).leader.equals(listofRole.get(i))) {


                            chosenEmpire = d;
                            count++;
                            fromEmpire =  Main.list.get(d).role.getAsMention();

                        }

                    }
                }

                if (count > 1) {


                    EmbedBuilder alpha = new EmbedBuilder();
                    alpha.setTitle("Too many roles!");
                    alpha.setColor(0xed4c37);
                    alpha.setDescription("You have too many leadership roles");
                    alpha.setFooter("Type .info for list of commands");

                    event.getChannel().sendMessage(alpha.build()).queue();


                } else if (chosenEmpire == -1) {

                    EmbedBuilder alpha = new EmbedBuilder();
                    alpha.setTitle("Incorrect Roles!");
                    alpha.setColor(0xed4c37);
                    alpha.setDescription("You must have only one leadership role");
                    alpha.setFooter("Type .info for list of commands");

                    event.getChannel().sendMessage(alpha.build()).queue();

                } else {
                    if (arg.length > 1) {
                        StringBuilder sb = new StringBuilder();
                        for (int i = 1; i < arg.length; i++) {
                            if (i == arg.length - 1) {

                                sb.append(arg[i]);

                            } else {
                                sb.append(arg[i] + " ");
                            }
                        }

                        full = sb.toString();

                        Random rand = new Random();
                        boolean condition = false;
                        int target = -1;
                        int num = rand.nextInt(100);

                        System.out.println(num);





                        for (int i = 0; i < Main.list.size(); i++) {


                            if (full.equalsIgnoreCase(Main.list.get(i).role.getName())) {


                                condition = true;
                                target = i;
                                break;

                            }




                        }




                        if (condition == true) {


                            if(Main.list.get(chosenEmpire).end != null){


                                StringBuilder builder = new StringBuilder();
                                Calendar add1 = Calendar.getInstance();
                                add1.setTime(Main.list.get(chosenEmpire).end );
                                add1.add(Calendar.MINUTE,1);

                                endDate = add1.getTime().toString().toString();
                                endDate = endDate.substring(0,16);
                                endDate = endDate + " GMT";


                            }

                            if (num>=0 && num<2) { //2% chance for sabotage to not work

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date date = new Date();
                                Calendar c = Calendar.getInstance();
                                c.setTime(date);




                                if(Main.list.get(chosenEmpire).cooldown == false) {


                                    EmbedBuilder alpha = new EmbedBuilder();
                                    alpha.setColor(Main.list.get(chosenEmpire).role.getColor());
                                    alpha.setImage("https://media.giphy.com/media/KcW2FVEJnzKeiZkVI1/giphy.gif");
                                    alpha.setTitle("Nothing happen!");
                                    alpha.setDescription("Don't worry you can try again in 24hrs");
                                    alpha.setFooter("Type .info for list of commands");
                                    event.getChannel().sendMessage(alpha.build()).queue();
                                    Main.list.get(chosenEmpire).cooldown = true;
                                    Main.list.get(chosenEmpire).start = c.getTime();
                                    if(Main.announcement != null) {
                                        event.getGuild().getTextChannelById(Main.announcement.getId()).sendMessage(alpha.build()).queue();
                                    }

                                    c.add(Calendar.MINUTE,cooldownTime);
                                    Main.list.get(chosenEmpire).end = c.getTime();



                                }
                                else{

                                    EmbedBuilder alpha = new EmbedBuilder();
                                    alpha.setColor(Main.list.get(chosenEmpire).role.getColor());
                                    alpha.setTitle("Cool Down in Effect!");
                                    alpha.setImage("https://media.giphy.com/media/26BGD4XaoPO3zTz9K/giphy.gif");
                                    alpha.setDescription("Unable to sabotage during cool down!\nCool down last till:  " + endDate);
                                    alpha.setFooter("Type .info for list of commands");
                                    event.getChannel().sendMessage(alpha.build()).queue();


                                }

                            } else if (num >= 2 && num <62) { //60% Chance

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date date = new Date();
                                Calendar c = Calendar.getInstance();
                                c.setTime(date);


                                if(Main.list.get(chosenEmpire).cooldown == false) {

                                    EmbedBuilder alpha = new EmbedBuilder();
                                    alpha.setColor(Main.list.get(chosenEmpire).role.getColor());
                                    alpha.setImage("https://media.giphy.com/media/GwGXoeb0gm7sc/giphy.gif");
                                    alpha.setTitle("Sabotaged 1000 points!");
                                    alpha.setDescription(fromEmpire + " has deducted 1000 points from " + Main.list.get(target).role.getAsMention());
                                    alpha.setFooter("Type .info for list of commands");

                                    Main.list.get(target).score = Main.list.get(target).score - 1000;
                                    c.add(Calendar.MINUTE,cooldownTime);
                                    Main.list.get(chosenEmpire).cooldown = true;
                                    Main.list.get(chosenEmpire).end = c.getTime();
                                    if(Main.announcement != null) {
                                        event.getGuild().getTextChannelById(Main.announcement.getId()).sendMessage(alpha.build()).queue();
                                    }

                                    if (Main.list.get(target).score < 0) {

                                        Main.list.get(target).score = 0;

                                    }
                                    event.getChannel().sendMessage(alpha.build()).queue();

                                }
                                else{

                                        EmbedBuilder alpha = new EmbedBuilder();
                                        alpha.setColor(Main.list.get(chosenEmpire).role.getColor());
                                        alpha.setTitle("Cool Down in Effect!");
                                    alpha.setImage("https://media.giphy.com/media/26BGD4XaoPO3zTz9K/giphy.gif");
                                    alpha.setDescription("Unable to sabotage during cool down!\nCool down last till:  " + endDate);
                                    alpha.setFooter("Type .info for list of commands");
                                        event.getChannel().sendMessage(alpha.build()).queue();


                                }



                            } else if (num >= 62 && num < 92) { //30% Chance

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date date = new Date();
                                Calendar c = Calendar.getInstance();
                                c.setTime(date);



                                if(Main.list.get(chosenEmpire).cooldown == false) {

                                    EmbedBuilder alpha = new EmbedBuilder();
                                    alpha.setColor(Main.list.get(chosenEmpire).role.getColor());
                                    alpha.setTitle("Sabotaged 2000 points!");
                                    alpha.setImage("https://media.giphy.com/media/vSSdLSLbGIXio/giphy.gif");
                                    alpha.setDescription(fromEmpire + " deducted 2000 points from " + Main.list.get(target).role.getAsMention());
                                    alpha.setFooter("Type .info for list of commands");
                                    Main.list.get(target).score = Main.list.get(target).score - 2000;
                                    if(Main.announcement != null) {
                                        event.getGuild().getTextChannelById(Main.announcement.getId()).sendMessage(alpha.build()).queue();
                                    }

                                    c.add(Calendar.MINUTE,cooldownTime);
                                    Main.list.get(chosenEmpire).end = c.getTime();
                                    Main.list.get(chosenEmpire).cooldown = true;

                                    if (Main.list.get(target).score < 0) {

                                        Main.list.get(target).score = 0;

                                    }
                                    event.getChannel().sendMessage(alpha.build()).queue();
                                }
                                else{ // 7% Chance

                                    EmbedBuilder alpha = new EmbedBuilder();
                                    alpha.setColor(Main.list.get(chosenEmpire).role.getColor());
                                    alpha.setTitle("Cool Down in Effect!");
                                    alpha.setImage("https://media.giphy.com/media/26BGD4XaoPO3zTz9K/giphy.gif");
                                    alpha.setDescription("Unable to sabotage during cool down!\nCool down last till:  " + endDate);
                                    alpha.setFooter("Type .info for list of commands");
                                    event.getChannel().sendMessage(alpha.build()).queue();

                                }

                            } else {


                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date date = new Date();
                                Calendar c = Calendar.getInstance();
                                c.setTime(date);

                                if(Main.list.get(chosenEmpire).cooldown == false) {

                                    EmbedBuilder alpha = new EmbedBuilder();
                                    alpha.setColor(Main.list.get(chosenEmpire).role.getColor());
                                    alpha.setImage("https://media.giphy.com/media/wFmJu7354Csog/giphy.gif");
                                    alpha.setTitle("Sabotaged ALL POINTS!");
                                    alpha.setFooter("Type .info for list of commands");
                                    alpha.setDescription(fromEmpire + " deducted all points from " + Main.list.get(target).role.getAsMention());

                                    Main.list.get(target).score = 0;
                                    event.getChannel().sendMessage(alpha.build()).queue();
                                    c.add(Calendar.MINUTE,cooldownTime);
                                    Main.list.get(chosenEmpire).end = c.getTime();
                                    Main.list.get(chosenEmpire).cooldown = true;
                                    if(Main.announcement != null) {
                                        event.getGuild().getTextChannelById(Main.announcement.getId()).sendMessage(alpha.build()).queue();
                                    }
                                }
                                else{

                                    EmbedBuilder alpha = new EmbedBuilder();
                                    alpha.setColor(Main.list.get(chosenEmpire).role.getColor());
                                    alpha.setTitle("Cool Down in Effect!");
                                    alpha.setImage("https://media.giphy.com/media/26BGD4XaoPO3zTz9K/giphy.gif");
                                    alpha.setDescription("Unable to sabotage during cool down!\nCool down last till:  " + endDate);
                                    alpha.setFooter("Type .info for list of commands");
                                    event.getChannel().sendMessage(alpha.build()).queue();


                                }
                            }


                        } else {

                            EmbedBuilder alpha = new EmbedBuilder();
                            alpha.setTitle("Incorrect empire");
                            alpha.setColor(0xed4c37);
                            alpha.setDescription("- This empire does not exist \n- For a list of empires type ``.empire list``");
                            alpha.setFooter("Type .info for list of commands");
                            event.getChannel().sendMessage(alpha.build()).queue();


                        }
                    } else {

                        EmbedBuilder alpha = new EmbedBuilder();
                        alpha.setTitle("Missing Empire");
                        alpha.setColor(0xed4c37);
                        alpha.setDescription("- You forgot to mention the empire\n- The format is ``.sabotage <empire>``");
                        alpha.setFooter("Type .info for list of commands");


                        event.getChannel().sendMessage(alpha.build()).queue();


                    }


                }
            }
        }
        else if (arg[0].equalsIgnoreCase(Main.prefix + "remove") && arg.length > 0 && arg[1].equalsIgnoreCase("cooldown") && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {


            if (arg.length > 2) {
                StringBuilder sb = new StringBuilder();
                for (int i = 2; i < arg.length; i++) {
                    if (i == arg.length - 1) {

                        sb.append(arg[i]);

                    } else {
                        sb.append(arg[i] + " ");
                    }
                }

                full = sb.toString();

                boolean condition = false;

                for(int i = 0; i< Main.list.size(); i++){


                    if(Main.list.get(i).role.getName().equalsIgnoreCase(full)){

                        condition = true;
                        Main.list.get(i).cooldown = false;
                        event.getChannel().sendMessage("Successfully removed cool down").queue();


                    }

                }

                if (condition == false){

                    EmbedBuilder alpha = new EmbedBuilder();
                    alpha.setTitle("Incorrect empire");
                    alpha.setColor(0xed4c37);
                    alpha.setDescription("- The empire you have mentioned does not exist \n- Type ``.empire list`` to see a list of empires");
                    alpha.setFooter("Type .info for list of commands");
                    event.getChannel().sendMessage(alpha.build()).queue();

                }



            }
            else  {

                EmbedBuilder alpha = new EmbedBuilder();
                alpha.setTitle("Missing Empire");
                alpha.setColor(0xed4c37);
                alpha.setDescription("- Mention the empire you wish to sabotage!.\n- Format is ``.remove cooldown <empire>`` \n ");
                alpha.setFooter("Type .info for list of commands");
                event.getChannel().sendMessage(alpha.build()).queue();


            }



        }


        else if (arg[0].equalsIgnoreCase(Main.prefix + "increment") && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {

            boolean condition = true;
            int index = -1;
            if (arg.length > 2) {

                int score = Integer.parseInt(arg[1]);

                StringBuilder sb = new StringBuilder();
                for (int i = 2; i < arg.length; i++) {
                    if (i == arg.length - 1) {

                        sb.append(arg[i]);

                    } else {
                        sb.append(arg[i] + " ");
                    }
                }
                full = sb.toString();


                for (int i = 0; i < Main.list.size(); i++) {

                    if (Main.list.get(i).role.getName().equalsIgnoreCase(full)) {

                        Main.list.get(i).score = Main.list.get(i).score + score;
                        event.getChannel().sendMessage("Successfully incremented " + score + " points").queue();
                        condition = false;
                        break;


                    }

                }
            }
            else{

                EmbedBuilder alpha = new EmbedBuilder();
                alpha.setTitle("Incorrect Format");
                alpha.setColor(0xed4c37);
                alpha.setDescription("- You have typed the wrong format for increment \n- Format: ``.increment <number> <empire>``");
                alpha.setFooter("Type .info for list of commands");
                event.getChannel().sendMessage(alpha.build()).queue();
                condition = false;


            }
            if(condition == true){


                EmbedBuilder alpha = new EmbedBuilder();
                alpha.setTitle("Incorrect empire");
                alpha.setColor(0xed4c37);
                alpha.setDescription("- The empire you have mentioned does not exist \n- Type ``.empire list`` to see a list of empires");
                alpha.setFooter("Type .info for list of commands");
                event.getChannel().sendMessage(alpha.build()).queue();

            }

            }
        else if (arg[0].equalsIgnoreCase(Main.prefix + "decrement") && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {

            boolean condition = true;
            int index = -1;
            if (arg.length > 2) {

                int score = Integer.parseInt(arg[1]);

                StringBuilder sb = new StringBuilder();
                for (int i = 2; i < arg.length; i++) {
                    if (i == arg.length - 1) {

                        sb.append(arg[i]);

                    } else {
                        sb.append(arg[i] + " ");
                    }
                }
                full = sb.toString();


                for (int i = 0; i < Main.list.size(); i++) {

                    if (Main.list.get(i).role.getName().equalsIgnoreCase(full)) {

                        Main.list.get(i).score = Main.list.get(i).score - score;
                        if(Main.list.get(i).score < 0){

                            Main.list.get(i).score = 0;

                        }
                        event.getChannel().sendMessage("Successfully decremented " + score + " points").queue();
                        condition = false;
                        break;


                    }

                }
            }
            else{

                EmbedBuilder alpha = new EmbedBuilder();
                alpha.setTitle("Incorrect Format");
                alpha.setColor(0xed4c37);
                alpha.setDescription("- You have typed the wrong format for decrement \n- Format: ``.decrement <number> <empire>``");
                alpha.setFooter("Type .info for list of commands");
                event.getChannel().sendMessage(alpha.build()).queue();
                condition = false;


            }
            if(condition == true){


                EmbedBuilder alpha = new EmbedBuilder();
                alpha.setTitle("Incorrect empire");
                alpha.setColor(0xed4c37);
                alpha.setDescription("- The empire you have mentioned does not exist \n- Type ``.empire list`` to see a list of empires");
                alpha.setFooter("Type .info for list of commands");
                event.getChannel().sendMessage(alpha.build()).queue();

            }

        }

        else if (arg[0].equalsIgnoreCase(Main.prefix + "empire") && arg.length > 1 && arg[1].equalsIgnoreCase("list")) {

                if (Main.list.size() == 0) {

                    event.getChannel().sendMessage("No empires have been declared").queue();

                } else {
                    for (int i = 0; i < Main.list.size(); i++) {


                        fullEmp = (Main.list.size() - i) + ". " + Main.list.get(i).role.getName() + " (" + Main.list.get(i).role.getAsMention() + ")" + "\n" + fullEmp;

                    }
                    EmbedBuilder bravo = new EmbedBuilder();
                    bravo.setColor(0xebf57c);
                    bravo.setTitle("Empire List");
                    bravo.setDescription(fullEmp);
                    bravo.setFooter("Type .info for list of commands");
                    event.getChannel().sendMessage(bravo.build()).queue();
                }


            }
        else if (arg[0].equalsIgnoreCase(Main.prefix + "announcement") && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {

            if(b.size() ==1) {
                Main.announcement = b.get(0);
                event.getChannel().sendMessage("Successfully added the announcement channel").queue();
            }
            else{

                event.getChannel().sendMessage("Please mention 1 text channel only").queue();

            }
        }
        else if (arg[0].equalsIgnoreCase(Main.prefix + "test") && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {

            event.getChannel().sendMessage("+add empire <@&928354183072063498>").queue();
            event.getChannel().sendMessage("+add empire <@&928354006135369738>").queue();

            event.getChannel().sendMessage("+add leader <@&928354183072063498> <@&928354313095479296>").queue();
            event.getChannel().sendMessage("+add leader <@&928354006135369738> <@&928353721480519690>").queue();

            event.getChannel().sendMessage("+add channel <#860201674798923776>").queue();

            event.getChannel().sendMessage("+increment 100 Khanistan").queue();
            event.getChannel().sendMessage("+increment 200 Sonic Leader").queue();











        }
        else if (arg[0].equalsIgnoreCase(Main.prefix + "channel") && arg[1].equalsIgnoreCase("list")) {


                if (Main.permChannel.size() == 0) {

                    event.getChannel().sendMessage("No channels have been set").queue();

                } else {

                    for (int i = 0; i < Main.permChannel.size(); i++) {


                        fullEmp = (Main.permChannel.size() - i) + ". " + Main.permChannel.get(i).getAsMention() + "\n" + fullEmp;

                    }
                    EmbedBuilder bravo = new EmbedBuilder();
                    bravo.setColor(0xebf57c);
                    bravo.setTitle("Permitted Channel List");
                    bravo.setDescription(fullEmp);
                    bravo.setFooter("Type .info for list of commands");


                    event.getChannel().sendMessage(bravo.build()).queue();
                }
            }  else if (arg[0].equalsIgnoreCase(Main.prefix + "add") && arg.length > 1 && arg[1].equalsIgnoreCase("channel") && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {


                if (b.size() <= 0 || b.size() > 1) {

                    event.getChannel().sendMessage("Invalid Command, please add 1 existing text channel at a time").queue();

                } else {

                    boolean c = true;
                    for (int i = 0; i < Main.permChannel.size(); i++) {

                        if (Main.permChannel.get(i).equals(b.get(0))) {
                            c = false;
                            event.getChannel().sendMessage("This channel has already been added").queue();
                            break;

                        }

                    }
                    if (c == true) {

                        Main.permChannel.add(b.get(0));
                        event.getChannel().sendMessage("Channel added!").queue();

                    }
                }
            } else if (arg[0].equalsIgnoreCase(Main.prefix + "reset") && arg[1].equalsIgnoreCase("settings") && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {


                Main.list.removeAll(Main.list);
                Main.permChannel.removeAll(Main.permChannel);
                event.getChannel().sendMessage("Settings have been reset!").queue();


            } else if (arg[0].equalsIgnoreCase(Main.prefix + "remove") && arg[1].equalsIgnoreCase("channel") && event.getMember().hasPermission(Permission.ADMINISTRATOR)) {

                if (b.size() <= 0 || b.size() > 1) {

                    event.getChannel().sendMessage("Invalid Command, please add 1 channel to remove").queue();

                } else {
                    boolean c = true;
                    for (int i = 0; i < Main.permChannel.size(); i++) {

                        if (Main.permChannel.get(i).equals(b.get(0))) {

                            Main.permChannel.remove(i);
                            event.getChannel().sendMessage("Channel has been removed!").queue();
                            break;


                        } else if (i == Main.permChannel.size() - 1) {

                            event.getChannel().sendMessage("Invalid, channel does not exist").queue();

                        }
                    }
                }
            } else {


            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            System.out.println(c.getTime().toString());

            for(int i = 0; i<Main.list.size(); i++){

                if(Main.list.get(i).end != null) {

                    System.out.println("EMPIRE END TIME:" + Main.list.get(i).end);
                    System.out.println("EMPIRE CURRENT TIME:" + c.getTime());


                    if (c.getTime().after(Main.list.get(i).end)) {
                        System.out.println(c.getTime().toString());

                        Main.list.get(i).cooldown = false;
                        Main.list.get(i).end = null;

                    }
                }

            }



                boolean add = false;
                for (int i = 0; i < Main.permChannel.size(); i++) {
                    if (current.equals(Main.permChannel.get(i))) {

                        List<Role> roleList = event.getMember().getRoles();
                        for (int d = 0; d < Main.list.size(); d++) {

                            for (int z = 0; z < roleList.size(); z++) {

                                if (Main.list.get(d).role.equals(roleList.get(z))) {

                                    Main.list.get(d).score++;

                                    break;



                                }

                            }
                        }
                    }
                }
            }

    }
}



